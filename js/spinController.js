/**
 * Created by adobrianskiy on 24.11.15.
 */
$('#mainframe').ready(function(){
    showSpin();
});
function showSpin(){
    $('#mainframe').spin('flower', 'black');
}
function hideSpin(){
    $('#mainframe').spin(false);
}