function makeDefaultVariationsSelected(data) {
    data.categories.forEach(function(category){
        category.products.forEach(function(product){
            product.elements.forEach(function(element){
                var variations = element.elementVariations;
                var marked = false;
                for(var i = 0; i < variations.length; i++){
                    if(variations[i].default){
                        variations[i].selected = true;
                        marked = true;
                        break;
                    }
                }
                if(!marked && variations.length > 0){
                    variations[0].selected = true;
                }
            });
        });
    });
}

function getSelectedVariationIndex(element){
    var variations = element.elementVariations;
    for(var i = 0; i < variations.length; i++){
        if(variations[i].selected){
            return i;
        }
    }
    return -1;
}

function openProducts(){
    $("#newtonCategoryList").hide();
    $("#newtonProductList").show();
}
function openCategories() {
    $("#newtonCategoryList").show();
    $("#newtonProductList").hide();
}

//function openVariants(variant){
//    $('#newton-navigation-categories').hide();
//    $('#newton-navigation-products').hide();
//    $('#newton-navigation-elements').hide();
//    $('#newton-navigation-' + variant).show();
//}
//
//$(document).ready(function(){
//    var height = $('#newton-navigation').height();
//    $('#newton-navigation-categories').height(height);
//    $('#newton-navigation-products').height(height);
//    $('#newton-navigation-elements').height(height);
//});

