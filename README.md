# LIST OF TECHNOLOGIES #
1. **JavaScript** [codeacedemy](https://www.codecademy.com/en/tracks/javascript)
2. **AngularJS** [codeacedemy](https://www.codecademy.com/en/courses/learn-angularjs)
3. **LESS** [lesscss.org](http://lesscss.org/)
4. **Gulp** [tutorial](https://scotch.io/tutorials/automate-your-tasks-easily-with-gulp-js)
5. **Canvas** [habrahabr](http://habrahabr.ru/post/111308/)
6. **[Fabric.js](http://fabricjs.com/)** 

# GET STARTED #

## PREREQUISITE TECHNOLOGIES##

1. Node.js - you can find it [here](https://nodejs.org/en/).
2. Git - [here](http://git-scm.com/downloads) is link. The path to the installed folder should be in PATH variable

Reboot your PC or laptop 

## GLOBAL MODULES ##

1. Install gulp: Type "npm install -g gulp" in command prompt or terminal in IDE. (sudo necessary for ubuntu)
2. Install bower: Type "npm install -g bower" in command prompt or terminal in IDE.(sudo necessary for ubuntu)

## CLONE & CONFIGURE ##
### You can skip first 2 steps if you feel that you can work with git in other way ###
1. Download and install [SourceTree](https://www.sourcetreeapp.com/).
2. Open SourceTree, log in and clone project.
3. Run IDE (WebStorm, Intellij Idea) and select the project. After launching open terminal (press alt+f12) and type "npm install". (also you can run this command in command prompt, but you should be in the project root)
4. After that type "bower install"

## Running ##
1. Open terminal in IDE or command prompt inside the project folder and type "gulp"