app.directive('orderList', function() {
    return {
        restrict: 'E',
        templateUrl: 'angular/directives/views/orderList.html'
    };
});