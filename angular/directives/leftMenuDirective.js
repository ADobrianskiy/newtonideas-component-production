app.directive('leftMenu', function() {
  return { 
    restrict: 'E',
    templateUrl: 'angular/directives/views/leftMenu.html'
  }; 
});