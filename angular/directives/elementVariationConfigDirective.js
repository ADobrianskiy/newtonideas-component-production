app.directive('elementVariationConfig', function() {
  return { 
    restrict: 'E',
    $scope: {
      element: "="
    },
    templateUrl: 'angular/directives/views/elementVariationConfig.html'
  }; 
});