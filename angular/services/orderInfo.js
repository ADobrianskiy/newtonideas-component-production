app.factory('orderInfo', ['$http', function($http) {
    var dataUrl = window.configs.orderUrl;

    return $http.get(dataUrl).success(function(data) {
        return data;
    })
        .error(function(err) {
            return err;
        });
}]);