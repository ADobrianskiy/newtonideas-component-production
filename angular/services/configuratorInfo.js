app.factory('configuratorInfo', ['$http', function($http) { 
	var dataUrl = window.configs.dataUrl;
	
	return $http.get(dataUrl).success(function(data) {
		return data;
	})
	.error(function(err) {
		return err;
	}); 
}]);