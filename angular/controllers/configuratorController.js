app.controller('configuratorController',
    ['$scope', 'configuratorInfo', function($scope, configuratorInfo){
        $scope.configuratorInfo = {
            categories: []
        };

        var canvas = $scope.canvas =  new fabric.Canvas('backgroundCanvas');
        canvas.controlsAboveOverlay = true;
        canvas.selection = false;
        $scope.ChangingCategory = false;

        canvas.on({
            'object:moving': function(e) {
                e.target.variation.top = e.target.top;
                e.target.variation.left = e.target.left;
            }
        });

        canvas.on({
            'mouse:down': function(e) {
                var obj = $scope.canvas.getActiveObject();
                if(obj != null)
                {
                    var elem = $scope.canvas.getActiveObject();
                    var elem2 = $("*[href='#newton-element-" +  $scope.removeSpaces(elem.get('name') + "']"));
                    var elem3 = document.getElementById("newton-element-" + $scope.removeSpaces( elem.get('name')));
                    var attr = elem3.getAttribute("aria-expanded");
                    $('.nav-pills a[href="#element"]').tab('show');
                    if(attr == "false" || attr == null) {
                        elem2.click();
                    }
                }

            }
        });

        configuratorInfo.success(function(data){
            makeDefaultVariationsSelected(data);
            $scope.configuratorInfo = data;
            $scope.setCategory(data.categories[0]);


        });

        $scope.setCategory = function (category){
            if(category){
                $scope.activeCategory = category;
                $scope.selectedVariations = [];
                $scope.ChangingCategory = true;
                $scope.setProduct(category.products[0]);
            }
        };

        $scope.setProduct = function (product){
            if(product){
                $scope.activeProduct = product;
                $scope.selectedVariations = [];
                $scope.setElement(product.elements[0]);
                if($scope.ChangingCategory == false) {
                    $('.nav-tabs a[href="#element"]').tab('show');
                }
                else
                {
                    $scope.ChangingCategory = false;
                }
            }
        };

        $scope.setElement = function (element){
            if(element){
                $scope.activeElement = element;

                var i = getSelectedVariationIndex(element);
                if(i != -1) {
                    $scope.setElementVariation(element.elementVariations[i]);
                }
            }
        };

        $scope.setElementVariation = function (variation){
            if(variation){
                variation.selected = true;
                $scope.activeVariation = variation;
            }

            redrawCanvas($scope.canvas, $scope.activeProduct);
        };

        $scope.changeElementVariation = function (variation){
            if(variation){
                if($scope.activeVariation){
                    $scope.activeVariation.selected = false;
                }
                variation.selected = true;
                $scope.activeVariation = variation;
            }
            redrawCanvas($scope.canvas, $scope.activeProduct);
        };

        $scope.getTotalPrice = function() {
            var total = 0;
            if($scope.activeProduct){
                $scope.activeProduct.elements.forEach(function(element){
                    element.elementVariations.forEach(function(variation){
                        if(variation.selected === true){
                            total += Number(variation.price);
                        }

                    })
                })
                return total;
            }
        };

        $scope.download = function(){
            downloadFabric($scope.canvas, $scope.activeProduct.name);
        };

        $scope.removeSpaces = function (name){
            return name.replace(/\s/g, "_");
        }

        $scope.orderClick = function () {
            var newWindow = window.open("order.html");
        }

        $scope.canvasToJson = function() {

            var myJson = {   "version" : "$scope.configuratorInfo.version",
                "currency" : "$scope.configuratorInfo.currency",
                "order" : [{"category":"$scope.activeCategory", "product" : "$scope.activeProduct"  }]
            }
            return myJson;
        }

        $scope.openElementTab = function(){
            $("#element-tab").click();
        }

        /*canvas.on({'mouse:down': function(e) {
           if(!$("#element-tab").attr('aria-expanded'))  $scope.openElementTab();
            var elem = $("*[href='#newton-element-" + e.target.element.name + "']");
            if(elem.hasClass('collapsed')){
                elem.click();
            }
        }});*/
    }]);


